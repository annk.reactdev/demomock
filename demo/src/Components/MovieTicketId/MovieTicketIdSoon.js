import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import actCenima from '../../Redux/actionCenima/actCenima'
import "./MovieTicketIdSoon.scss"
import { useNavigate } from 'react-router-dom'

function MovieTicketIdSoon(props) {
    const nav = useNavigate()
    const idMovie = sessionStorage.getItem("idMovieTicket")
    useEffect(() => {
        props.GetDataCenima()
        props.MovieIdTicket(idMovie)
    }, [])

    const MovieDetail = props.dataCenima.lsDataCenima.movieCommingSoon?.find(n => n.id == idMovie)
    const SelectionTicket = (date, days, showtime, nameCenima) => {
        sessionStorage.setItem("dates", date)
        sessionStorage.setItem("days", days)
        sessionStorage.setItem("showtime", showtime)
        sessionStorage.setItem("idCenimaTicket", nameCenima)
        nav('/selectionticket')
    }
    return (
        <div className='MovieTicketIdSoon'>
            <div className='Container'>
                {
                    MovieDetail ? <div className='infoMovie'>
                        <img src={MovieDetail.imageLandscape} />
                        <div className='content'>
                            <div>
                                <h2>{MovieDetail.name}</h2>
                                <h3>{MovieDetail.subName}</h3>
                                <p>Khởi chiếu: <span>{MovieDetail.startdate}</span></p>
                                <p>Lượt xem: <span>{MovieDetail.views}</span></p>
                                <div
                                    className="flex-grow"
                                    dangerouslySetInnerHTML={{ __html: MovieDetail.description }}
                                    style={{ color: 'white' }}
                                />
                            </div>
                        </div>
                    </div> : ""
                }
                {
                    props.dataCenima.lsMovieId?.map((n, i) => {
                        let nameCenima = n.name
                        return (
                            <div key={i} className='ThongtinRap' >
                                <h3>{n.name}</h3>
                                {
                                    n.dates?.map((n1, i1) => {
                                        let sub = n1.bundles.find(n => n.caption == "sub")
                                        let longtieng = n1.bundles.find(n => n.caption == "voice")
                                        let date = n1.showDate
                                        let days = n1.dayOfWeekLabel
                                        return (
                                            <div key={i1} className='suatchieu'>
                                                <h2>{n1.dayOfWeekLabel} {n1.showDate}</h2>
                                                <div className='phude'>
                                                    <p>{sub?.version ? "2D" : ""} {sub?.caption ? "Phụ đề" : ""}</p>
                                                    <div className='giochieu'>
                                                        {sub?.sessions.map((n2, i2) => {
                                                            let showtime = n2.showTime
                                                            return <button onClick={() => { SelectionTicket(date, days, showtime, nameCenima) }} key={i2}>{n2.showTime}</button>
                                                        })}
                                                    </div>
                                                </div>
                                                <div className='longtieng'>
                                                    <p>{longtieng?.version ? "2D" : ""} {longtieng?.caption ? "L.Tiếng" : ""}</p>
                                                    <div className='giochieu'>
                                                        {longtieng?.sessions.map((n3, i3) => {
                                                            let showtime = n3.showTime
                                                            return <button onClick={() => { SelectionTicket(date, days, showtime, nameCenima) }} key={i3}>{n3.showTime}</button>
                                                        })}
                                                    </div>
                                                </div>
                                            </div>
                                        )
                                    })
                                }

                            </div>
                        )
                    })
                }

            </div>
        </div>
    )
}
const mapStateToProps = (globalState) => {
    return {
        dataCenima: globalState.dataManage
    }
}
const mapDispatchToProps = (dispath) => {
    return {
        GetDataCenima: () => {
            dispath({
                type: actCenima.GET_DATA_CENIMA
            })
        },
        MovieIdTicket: (idMovie) => {
            dispath({
                type: actCenima.GET_MOVIE_ID,
                payload: idMovie
            })
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(MovieTicketIdSoon)
