import React from 'react'
import "./Footer.scss"

export default function Footer() {
    return (
        <div className='Footer'>
            <div>
                <img src='./img/logoheader.png' />
                <div>
                    <h1>Cenima</h1>
                </div>
            </div>
            <hr />
            <p>© 2015 BHD Star Cineplex</p>
        </div>
    )
}
