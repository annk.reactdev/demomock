import React, { useEffect, useState } from 'react'
import "./SelectionTicket.scss"
import { connect } from "react-redux";
import actCenima from '../../../Redux/actionCenima/actCenima';

function SelectionTicket(props) {
    const days = sessionStorage.getItem("days")
    const dates = sessionStorage.getItem("dates")
    const showtime = sessionStorage.getItem("showtime")
    const cenima = sessionStorage.getItem("idCenimaTicket")
    const idMovieTicket = sessionStorage.getItem("idMovieTicket")

    useEffect(() => {
        props.GetBookingDetail()
        props.GetBooking()
    }, [])
    const movie = props.dataCenima.lsBooking.movies?.find(n => n.id == idMovieTicket)

    const Up = (TypeCode) => {
        // const updateLsTicket = lsTicket.map((n) => {
        //     if (n.ticketTypeCode === TypeCode) {
        //         return {
        //             ...n,
        //             defaultQuantity: n.defaultQuantity * 1 + 1
        //         };
        //     }
        //     return n
        // })
        // setLsTicket(updateLsTicket)
    }

    const Down = (TypeCode) => {
        // const updateLsTicket = lsTicket.map((n) => {
        //     if (n.ticketTypeCode === TypeCode && n.defaultQuantity > 0) {
        //         return {
        //             ...n,
        //             defaultQuantity: n.defaultQuantity * 1 - 1
        //         };
        //     }
        //     return n
        // })
        // setLsTicket(updateLsTicket)
    }
    return (
        <div className='SelectionTicket'>
            <div className='Container'>
                <div className='selectionTicket'>
                    <h1>CHỌN VÉ/THỨC ĂN</h1>
                    <div className='loaive'>
                        <table>
                            <thead>
                                <tr>
                                    <th>Loại vé</th>
                                    <th>Số lượng </th>
                                    <th>Giá(VNĐ)</th>
                                    <th>Tổng(VNĐ)</th>
                                </tr>
                            </thead>
                            <tbody className='ticketType'>
                                {
                                    props.dataCenima.lsBookingDetail.ticket?.map((n, i) => {
                                        let ticketTypeCode = n.ticketTypeCode
                                        return (
                                            <tr key={i}>
                                                <td>
                                                    <p>{n.name}</p>
                                                    <p>{n.description}</p>
                                                </td>
                                                <td>
                                                    <button onClick={() => { Down(ticketTypeCode) }} style={{ padding: "5px", fontWeight: "bold", margin: "5px" }}>
                                                        -
                                                    </button>
                                                    <input
                                                        key={i}
                                                        style={{ width: "40px", textAlign: "center" }}
                                                        value={0}
                                                        type='number'

                                                    />
                                                    <button onClick={() => { Up(ticketTypeCode) }} style={{ padding: "5px", fontWeight: "bold", margin: "5px" }}>
                                                        +
                                                    </button>
                                                </td>
                                                <td>{n.price}</td>
                                                <td>Tổng(VNĐ)</td>
                                            </tr>
                                        )
                                    })
                                }

                            </tbody>

                            <thead>
                                <tr>
                                    <th>Combo</th>
                                    <th>Số lượng </th>
                                    <th>Giá(VNĐ)</th>
                                    <th>Tổng(VNĐ)</th>
                                </tr>
                            </thead>

                            {
                                props.dataCenima.lsBookingDetail.consession?.map((n, i) => {
                                    return (
                                        <tbody className='Combo' key={i}>
                                            {
                                                n.concessionItems.map((n1, i1) => {
                                                    return <tr key={i1}>
                                                        <td >

                                                            <div><img src={n1.imageUrl} /> {n1.description}</div>

                                                        </td>
                                                        <td>
                                                            <button style={{
                                                                padding: "5px",
                                                                fontWeight: "bold",
                                                                margin: "5px"
                                                            }}>-</button>
                                                            <input type='number' value={0} style={{ width: "40px", textAlign: "center" }}></input>
                                                            <button style={{
                                                                padding: "5px",
                                                                fontWeight: "bold",
                                                                margin: "5px"
                                                            }}>+</button>
                                                        </td>
                                                        <td>{n1.price}</td>
                                                        <td>Tổng(VNĐ)</td>
                                                    </tr>
                                                })
                                            }
                                        </tbody>
                                    )


                                })
                            }

                        </table>
                    </div>
                </div>
                {
                    movie ? <div className='infoTicket'>
                        <div>
                            <img src={movie.imageLandscape} />
                        </div>
                        <div>
                            <h2 className='name'>{movie.name}</h2>
                            <h2 className='subname'>{movie.subName}</h2>
                        </div>
                        <div>
                            <p><span>Rạp: </span>{cenima}</p>
                            <p><span>Suất chiếu: </span>{showtime} | {days}, {dates}</p>
                            <p><span>Combo: </span>zjhdf,hj</p>
                            <p><span>Ghế: </span>zjhdf,hj</p>
                            <p><span>Tổng: </span>zjhdf,hj</p>
                        </div>
                        <div className='bnt'>
                            <button>Tiếp Tục</button>
                        </div>
                    </div> : ""
                }
            </div>
        </div>
    )
}
const mapStateToProps = (globalState) => {
    return {
        dataCenima: globalState.dataManage
    }
}
const mapDispatchToProps = (dispath) => {
    return {
        GetBookingDetail: () => {
            dispath({
                type: actCenima.GET_BOOKING_DETAIL
            })
        },
        GetBooking: () => {
            dispath({
                type: actCenima.GET_BOOKING
            })
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(SelectionTicket);
