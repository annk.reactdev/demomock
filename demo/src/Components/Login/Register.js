import React, { useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import "./Register.scss"

export default function Register() {
    const nav = useNavigate()

    const [Email, setEmail] = useState("")
    const [Name, setName] = useState("")
    const [Password, setPassword] = useState("")
    const [Role, setRole] = useState("")

    // const [emailErr, setEmailErr] = useState("")
    // const [nameErr, setNameErr] = useState("")
    // const [passwordErr, setPasswordErr] = useState("")

    const DangKy = () => {
        const re = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
        if (re.test(Email) == false) {
            alert("Email không hợp lệ")
            return
        }
        else {
            if (Name == "") {
                alert("Name không hợp lệ")
                return
            }
            else {
                if (Password.length < 8) {
                    alert("Password không hợp lệ")
                    return
                }
                else {
                    if (Role == "") {
                        alert("Vui lòng nhập Role")
                        return
                    }
                }
            }
        }
        const requesRegister = {
            Email: Email,
            Name: Name,
            Password: Password,
            Role: Role
        }
        fetch("https://teachingserver.org/U2FsdGVkX18MaY1VB6bVfvVBm0wdPflO/user/user", {
            headers: {
                'accept': 'application/json',
                'Content-Type': 'application/json',
            },
            method: "POST",
            body: JSON.stringify(requesRegister)
        }).then(res => {
            if (res.status === 200) {
                alert("Đăng ký thành công")
                nav("/login")
            }
        })
    }
    return (
        <div className='Register'>
            <div className='Container'>
                <div className='tab'>
                    <div className='tab1'>
                        <Link to={"/login"}>Đăng Nhập</Link >
                    </div>
                    <div className='tab2'>
                        <Link to={"/register"} style={{ color: "salmon" }}>Đăng Ký</Link>
                    </div>
                </div>
                <div className='Content'>
                    <input value={Email} onChange={(e) => { setEmail(e.target.value) }} placeholder='Email' /><br />
                    <input value={Name} onChange={(e) => { setName(e.target.value) }} placeholder='Name' /><br />
                    <input value={Password} onChange={(e) => { setPassword(e.target.value) }} placeholder='Password' /><br />
                    <input value={Role} onChange={(e) => { setRole(e.target.value) }} placeholder='Role' /><br />
                    <p>Tôi đã đọc và đồng ý với <span>CHÍNH SÁCH</span> của chương trình.</p>
                    <button onClick={DangKy}>Đăng Ký</button>
                </div>
            </div>
        </div>
    )
}
