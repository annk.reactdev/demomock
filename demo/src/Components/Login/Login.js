import React, { useEffect, useState } from 'react'
import "./Login.scss"
import { Link, useNavigate } from 'react-router-dom'


export default function Login(props) {
    const nav = useNavigate()
    const [Email, setEmail] = useState("")
    const [Password, setPassword] = useState("")

    const Submit = () => {
        if (Email == "") {
            alert("Vui lòng nhập email")
            return
        }
        else {
            if (Password == "") {
                alert("Vui lòng nhập mật khẩu")
                return
            }
        }
        const requesLogin = {
            Email: Email,
            Password: Password
        }
        fetch("https://teachingserver.org/U2FsdGVkX18MaY1VB6bVfvVBm0wdPflO/user/Login", {
            headers: {
                'accept': 'application/json',
                'Content-Type': 'application/json',
            },
            method: "POST",
            body: JSON.stringify(requesLogin)
        }).then(res => {
            if (res.status === 200) {
                localStorage.setItem("Email", Email)
                nav("/")
            }
            else {
                alert("Email/Password không đúng")
            }
        })

    }

    return (
        <div className='Login'>
            <div className='Container'>
                <div className='tab'>
                    <div className='tab1'>
                        <Link to={"/login"} style={{ color: "salmon" }}>Đăng Nhập</Link >
                    </div>
                    <div className='tab2'>
                        <Link to={"/register"}>Đăng Ký</Link>
                    </div>
                </div>
                <div className='Content'>
                    <p>Vui lòng đăng nhập trước khi mua vé để tích luỹ điểm, cơ hội nhận thêm nhiều ưu đãi từ chương trình thành viên Cinema.</p>
                    <input value={Email} onChange={(event) => { setEmail(event.target.value) }} className='ip1' placeholder='Email' /><br />
                    <input value={Password} onChange={(event) => { setPassword(event.target.value) }} className='ip2' placeholder='Mật Khẩu' /><br />
                    <button onClick={Submit}>Đăng Nhập</button>
                </div>
            </div>
        </div >
    )
}


