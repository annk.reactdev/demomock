import React, { useState } from 'react'
import Login from '../User/Login';
import Register from '../User/Register';

export default function Popup() {
    
const [showPopup, setShowPopup] = useState(false);

  const handleShowPopup = () => {
    setShowPopup(true);
  };

  const handleClosePopup = () => {
    setShowPopup(false);
  };

  const [activeTab, setActiveTab] = useState('Login');
  const handleTabChange = (tab) => {
    setActiveTab(tab);
  };

  return (
    <div className='Popup'>
     <button
        type="button"
        className="btnLogin"
        onClick={handleShowPopup}
      >
        Mua vé
      </button>

      {showPopup && (
        
  <div className="relative max-w-xl rounded-lg bg-gray-100 p-6 shadow-sm mx-auto">
    <button
      type="button"
      className="absolute -end-1 -top-1 rounded-full border border-gray-200 bg-white p-1 text-gray-400"
      onClick={handleClosePopup}
    >
      <span className="sr-only">Close</span>
      <svg
        xmlns="http://www.w3.org/2000/svg"
        className="h-3 w-3"
        viewBox="0 0 20 20"
        fill="currentColor"
      >
        <path
          fillRule="evenodd"
          d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
          clipRule="evenodd"
        />
      </svg>
    </button>
  <div className="px-6 py-5">
  <div className="tabMenu">
        <button
          className={activeTab === 'A' ? 'active' : ''}
          onClick={() => handleTabChange('Login')}
        >
          Login | 
        </button>
        <button
          className={activeTab === 'B' ? 'active' : ''}
          onClick={() => handleTabChange('Register')}
        >
          Register
        </button>
      </div>

      <div className="tabContent">
        {activeTab === 'Login' && <Login />}
        {activeTab === 'Register' && <Register />}
      </div>
  </div>

  </div>

      )}

      

    </div>
  )
}
