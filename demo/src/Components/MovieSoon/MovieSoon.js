import React, { useEffect } from 'react'
import "./MovieSoon.scss"
import { connect } from 'react-redux'
import actCenima from '../../Redux/actionCenima/actCenima'
import { Link, useNavigate } from 'react-router-dom'
import SlideShow from '../SlideShow/SlideShow'

function MovieSoon(props) {
    const nav = useNavigate()
    useEffect(() => {
        props.GetDataCenima()
    }, [])
    const lsmovieCommingSoon = props.dataCenima.lsDataCenima.movieCommingSoon
    const GetMovieId = (id) => {
        sessionStorage.setItem("idMovieTicket", id)
        nav("/movieticketidsoon")
    }
    return (
        <div className='MovieSoon'>
            <SlideShow />
            <div className='tab'>
                <div className='tab1'>
                    <Link to={"/"}>Phim Đang Chiếu</Link >
                </div>
                <div className='tab2'>
                    <Link to={"/moviesoon"} style={{ color: "salmon" }}>Phim Sắp Chiếu</Link>
                </div>
            </div>
            <div className='Container'>
                {
                    lsmovieCommingSoon?.map((n, i) => {
                        return (
                            <div key={i} className='mainCarMovie'>
                                <div>
                                    <img src={n.imagePortrait} />
                                    <h3>{n.name}</h3>
                                    <p>{n.subName}</p>
                                    <button value={n.id} onClick={(e) => { GetMovieId(e.target.value) }}>MUA VÉ</button>
                                </div>
                            </div>
                        )
                    })
                }
            </div>
        </div>
    )
}
const mapStateToProps = (globalState) => {
    return {
        dataCenima: globalState.dataManage
    }
}
const mapDispatchToProps = (dispath) => {
    return {
        GetDataCenima: () => {
            dispath({
                type: actCenima.GET_DATA_CENIMA
            })
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(MovieSoon);
