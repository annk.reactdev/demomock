import React, { useEffect, useState } from 'react'
import "./Header.scss"
import { useNavigate } from 'react-router-dom'
import { connect } from "react-redux";
import actCenima from '../../Redux/actionCenima/actCenima';
import { Sticky } from "react-sticky";

function Header(props) {
    const email = localStorage.getItem("Email")
    useEffect(() => {
        props.GetUser()
    }, [])
    const nav = useNavigate()
    const Login = () => {
        nav("login")
    }
    const Logout = () => {
        localStorage.removeItem("Email")
        nav("/")
    }
    const userName = props.data.lsUser.find(n => n.Email == email)
    return (
        <div className='Header'>
            <div className='Container'>
                <div className='Logo'>
                    <img href="/" src="./img/logoheader.png" />
                    <input placeholder='Search Phim' />
                    {
                        userName ? <button onClick={Logout}>Đăng Xuất</button> : <button onClick={Login}>Đăng Nhập</button>
                    }
                </div>
                <div className='NavBar'>
                    <ul>
                        <li><a href="/">Phim</a></li>
                        <li><a href="ticket">Mua vé</a></li>
                        <li><a href="ticketcenima">Rạp</a></li>
                        <li>{userName ? <a href="user">{userName.Name}</a> : <a href="user">Thành viên</a>}</li>
                    </ul>
                </div>

            </div>
        </div>
    )
}
const mapStateToProps = (globalState) => {
    return {
        data: globalState.dataManage
    }
}
const mapDispatchToProps = (dispath) => {
    return {
        GetUser: () => {
            dispath({
                type: actCenima.GET_USER
            })
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Header);
