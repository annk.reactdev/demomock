import actCenima from "../actionCenima/actCenima";

const initialState = {
    //danh sách tất cả phim đang chiếu và sắp chiếu
    lsDataCenima: [],

    //danh sách rạp phim 
    lsCenimaAll: [],

    //lấy ra dữ liệu của rạp phim theo ID
    lsCenimaID: [],

    //danh sách người dùng đăng nhập 
    lsUser: [],

    //lấy ra dữ liệu của phim theo ID
    lsMovieId: [],

    //danh sách phim, rạp, suất chiếu đang được bán
    lsBooking: [],

    //lấy ra giờ chiếu theo ID của phim 
    lsDates: "",

    //lấy ra giờ chiếu theo id phim bên chọn rạp trước
    lsDatesMovie: "",

    //danh sách loại vé, combo, chỗ ngồi
    lsBookingDetail: [],
}
const rdcCenima = (state = initialState, { type, payload }) => {
    switch (type) {
        case actCenima.SET_DATA_CENIMA:
            return {
                ...state,
                lsDataCenima: payload,
            }
        case actCenima.SET_CENIMA_ALL:
            return {
                ...state,
                lsCenimaAll: payload,
            }
        case actCenima.SET_CENIMA_ID:
            return {
                ...state,
                lsCenimaID: payload,
            }
        case actCenima.SET_USER:
            return {
                ...state,
                lsUser: payload,
            }
        case actCenima.SET_MOVIE_ID:
            return {
                ...state,
                lsMovieId: payload,
            }
        case actCenima.SET_BOOKING:
            return {
                ...state,
                lsBooking: payload,
            }
        case actCenima.SET_MOVIE_DATES:
            {
                let temp = state.lsMovieId.find(n => {
                    return n.id == payload
                })
                return {
                    ...state,
                    lsDates: temp
                }
            }
        case actCenima.SET_MOVIE_DATES_1:
            {
                let temp = state.lsCenimaID.find(n => {
                    return n.id == payload
                })
                return {
                    ...state,
                    lsDatesMovie: temp
                }
            }
        case actCenima.SET_BOOKING_DETAIL:
            return {
                ...state,
                lsBookingDetail: payload,
            }
        default:
            return state
    }
}
export default rdcCenima;