import { call, put, take, takeEvery } from "redux-saga/effects";
import actCenima from "../actionCenima/actCenima";


//danh sách loại vé, combo, chỗ ngồi
async function GetBookDetailAPI(params) {
    let res = await fetch("https://teachingserver.org/U2FsdGVkX18MaY1VB6bVfvVBm0wdPflO/cinema/booking/detail")
    let data = await res.json()
    return data
}
function* GetBookingDetail({ type, payload }) {
    let data = yield call(GetBookDetailAPI)
    yield put({
        type: actCenima.SET_BOOKING_DETAIL,
        payload: data
    })
}


//danh sách tất cả rạp phim 
async function GetCenimaAllAPI(params) {
    let res = await fetch("https://teachingserver.org/U2FsdGVkX18MaY1VB6bVfvVBm0wdPflO/cinema/cinemas")
    let data = await res.json()
    return data
}
function* GetCenimaAll({ type, payload }) {
    let data = yield call(GetCenimaAllAPI)
    yield put({
        type: actCenima.SET_CENIMA_ALL,
        payload: data
    })
}

async function GetCenimaIdAPI(idCenima) {
    let res = await fetch(`https://teachingserver.org/U2FsdGVkX18MaY1VB6bVfvVBm0wdPflO/cinema/cinemas/${idCenima}`)
    let data = await res.json()
    return data
}
function* GetCenimaId({ type, payload }) {
    let data = yield call(GetCenimaIdAPI, payload)
    yield put({
        type: actCenima.SET_CENIMA_ID,
        payload: data
    })
}

//danh sách phim, rạp, suất chiếu đang được bán
async function GetAPIBooking(params) {
    let res = await fetch("https://teachingserver.org/U2FsdGVkX18MaY1VB6bVfvVBm0wdPflO/cinema/booking")
    let data = await res.json()
    return data
}
function* GetBooking({ type, payload }) {
    let data = yield call(GetAPIBooking)
    yield put({
        type: actCenima.SET_BOOKING,
        payload: data
    })
}

//lấy ra dữ liệu của phim theo ID
async function GetIdAPI(id) {
    let res = await fetch(`https://teachingserver.org/U2FsdGVkX18MaY1VB6bVfvVBm0wdPflO/cinema/movie/${id}`)
    let data = await res.json()
    return data
}
function* GetMovieId({ type, payload }) {
    let data = yield call(GetIdAPI, payload)
    yield put({
        type: actCenima.SET_MOVIE_ID,
        payload: data
    })
}

//danh sách người dùng đăng nhập 
async function GetAPIUser(params) {
    let res = await fetch("https://teachingserver.org/U2FsdGVkX18MaY1VB6bVfvVBm0wdPflO/user/user")
    let data = await res.json()
    return data
}
function* GetUser({ type, payload }) {
    let data = yield call(GetAPIUser)
    yield put({
        type: actCenima.SET_USER,
        payload: data
    })
}

//danh sách tất cả phim đang chiếu và sắp chiếu
async function GetAPI(params) {
    let res = await fetch("https://teachingserver.org/U2FsdGVkX18MaY1VB6bVfvVBm0wdPflO/cinema/nowAndSoon")
    let data = await res.json()
    return data;

}
function* GetDataCenima({ type, payload }) {
    let data = yield call(GetAPI)
    yield put({
        type: actCenima.SET_DATA_CENIMA,
        payload: data
    })
}
function* mySaga(params) {
    yield takeEvery(actCenima.GET_DATA_CENIMA, GetDataCenima)
    yield takeEvery(actCenima.GET_USER, GetUser)
    yield takeEvery(actCenima.GET_MOVIE_ID, GetMovieId)
    yield takeEvery(actCenima.GET_BOOKING, GetBooking)
    yield takeEvery(actCenima.GET_CENIMA_ALL, GetCenimaAll)
    yield takeEvery(actCenima.GET_CENIMA_ID, GetCenimaId)
    yield takeEvery(actCenima.GET_BOOKING_DETAIL, GetBookingDetail)
}
export default mySaga;