const actCenima = {
    //danh sách tất cả phim đang chiếu và sắp chiếu
    GET_DATA_CENIMA: "GET_DATA_CENIMA",
    SET_DATA_CENIMA: "SET_DATA_CENIMA",

    //danh sách rạp phim 
    GET_CENIMA_ALL: "GET_CENIMA_ALL",
    SET_CENIMA_ALL: "SET_CENIMA_ALL",

    //lấy ra dữ liệu của rạp phim theo ID của rạp 
    GET_CENIMA_ID: "GET_CENIMA_ID",
    SET_CENIMA_ID: "SET_CENIMA_ID",

    //danh sách người dùng đăng nhập 
    GET_USER: "GET_USER",
    SET_USER: "SET_USER",

    //lấy ra dữ liệu của phim theo ID
    GET_MOVIE_ID: "GET_MOVIE_ID",
    SET_MOVIE_ID: "SET_MOVIE_ID",

    //danh sách phim, rạp, suất chiếu đang được bán
    GET_BOOKING: "GET_BOOKING",
    SET_BOOKING: "SET_BOOKING",

    //lấy ra giờ chiếu theo ID của phim 
    SET_MOVIE_DATES: "SET_MOVIE_DATES",

    //lấy ra giờ chiếu theo id phim bên chọn rạp trước
    SET_MOVIE_DATES_1: "SET_MOVIE_DATES_1",

    //danh sách loại vé, combo, chỗ ngồi
    GET_BOOKING_DETAIL: "GET_BOOKING_DETAIL",
    SET_BOOKING_DETAIL: "SET_BOOKING_DETAIL",
}
export default actCenima;