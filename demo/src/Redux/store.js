import { applyMiddleware, combineReducers, createStore } from "redux";
import rdcCenima from "./reducerCenima/rdcCenima";
import createSaga from "redux-saga";
import middleSaga from "./saga/middleSaga";

const saga = createSaga()
const globalState = combineReducers({
    dataManage: rdcCenima,
})
const store = createStore(
    globalState,
    applyMiddleware(saga)
)
export default store;
saga.run(middleSaga)