import React from 'react'
import { BrowserRouter, Link, Route, Routes } from "react-router-dom";
import Home from './Components/Home/Home';
import Header from './Components/Header/Header';
import Footer from './Components/Footer/Footer';
import "./App.scss"
import MovieSoon from './Components/MovieSoon/MovieSoon';
import Login from './Components/Login/Login';
import Register from './Components/Login/Register';
import MovieTicketId from './Components/MovieTicketId/MovieTicketId';
import MovieTicketIdSoon from './Components/MovieTicketId/MovieTicketIdSoon';
import Ticket from './Components/Ticket/Ticket';
<<<<<<< HEAD
import TicketCenima from './Components/Ticket/TicketCenima';
import SelectionTicket from './Components/Ticket/SelectionTicket/SelectionTicket';
=======
>>>>>>> 89d97e34651582341156b1d75d9907510887e023

export default function App() {
  return (
    <div className='App'>
      <BrowserRouter>
        <Header />
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='moviesoon' element={<MovieSoon />} />
          <Route path='login' element={<Login />} />
          <Route path='register' element={<Register />} />
          <Route path='movieticketid' element={<MovieTicketId />} />
          <Route path='movieticketidsoon' element={<MovieTicketIdSoon />} />
          <Route path='ticket' element={<Ticket />} />
          <Route path='ticketcenima' element={<TicketCenima />} />
          <Route path='selectionticket' element={<SelectionTicket />} />
        </Routes >
        <Footer />
      </BrowserRouter >
    </div >
  )
}
